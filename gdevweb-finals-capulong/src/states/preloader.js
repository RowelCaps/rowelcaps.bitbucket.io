class Preloader extends Phaser.State {
    constructor() {
        super();

        this.config = {
            landscape: {
                background: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.5, 
                    angle: 90
                },
                title: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.4
                },
                mobile: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.7
                },
                loading: {
                    x: game.baseResolution.max * 0.325,
                    y: game.baseResolution.min * 0.5
                }
            },
            portrait: {
                background: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.5,
                    angle: 0
                },
                title: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.4
                },
                mobile: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.6
                },
                loading: {
                    x: game.baseResolution.min * 0.25,
                    y: game.baseResolution.max * 0.45
                }
            }
        }
    }

    preload() {
        // initialize orientation
        getViewportDimensions();
        resizeCanvas();
        
        // Disable auto pause on window blur
        if (game && game.stage) {
            game.stage.disableVisibilityChange = true;
            window.document.addEventListener('pause', function() {
                if(!game.paused) {
                    game.isPaused();
                }
            }, false);
            window.document.addEventListener('resume', function() {
                setTimeout(function() {
                    if(game.paused) {
                        game.gameResumed();
                    }
                }, 0);
            }, false);
        }

        this.game.load.image('bg', 'assets/bg.png');
    }

    create() {
        // game container (for resizing)
        game.container = game.world.addChild (new Phaser.Group (game));
        game.container.reorientLayout = this.reorientLayout.bind(this);

        // preloader container
        this.container = new Phaser.Group (game);
        game.container.add (this.container);

        // add black background
        this.background = this.container.add (this.game.add.image(0, 0, 'bg'));
        this.background.anchor.setTo (0.5, 0.5);        

        var style = {
            font: 'Arial',
            fontSize: 32,
            fill: "#ffffff",
            align: "center"
        };
        
        // add preloader text
        this.titleText = this.container.add (this.game.add.text (this.game.world.centerX, this.game.height * 0.25, game.strings["TEST"], style));
        this.titleText.anchor.setTo (0.5, 0.5);

        // add text for checking if mobile or desktop
        this.mobileText = this.container.add (this.game.add.text (this.game.world.centerX, this.game.height * 0.75, game.isMobile ? game.strings["IS_MOBILE"] : game.strings["IS_DESKTOP"], style));
        this.mobileText.anchor.setTo (0.5, 0.5);
        this.mobileText.fontSize = 20;

        this.createLoadingBar ();
        this.startLoading ();

        // update orientation
        adaptGameToOrientation();
        assignOrientationChangeHandlers();

    }

    createLoadingBar () {
        this.frame = this.game.add.graphics ();
        this.frame.anchor.setTo (0.5, 0.5);
        this.frame.lineStyle(2, 0xffffff, 1);
        this.frame.drawRect(0, 0, game.baseResolution.min * 0.5, game.baseResolution.max * 0.05);
        
        this.bar = this.game.add.graphics ();
        this.bar.anchor.setTo (0.5, 0.5);
        this.bar.beginFill (0xffffff, 1);
        this.bar.drawRect(0, 0, game.baseResolution.min * 0.5, game.baseResolution.max * 0.05);
        this.bar.endFill ();

        this.loadingGroup = this.container.add (this.game.add.group ());
        this.loadingGroup.add (this.frame);
        this.loadingGroup.add (this.bar);

        this.bar.scale.x = 0;
    }   

    startLoading () {
        this.tweenScale = this.game.add.tween (this.bar.scale).to ({x: 1}, 100, Phaser.Easing.Linear.None, true);
        this.tweenScale.onComplete.addOnce (function () {
            this.game.state.clearCurrentState();
            this.game.state.start('game');
        }, this)
    }

    update () {
        
    }

    reorientLayout (orientation) {
        this.orient = orientation;

        console.log ("orientation: " + orientation)

        this.background.angle = this.config[orientation].background.angle;
        this.background.x = this.config[orientation].background.x;
        this.background.y = this.config[orientation].background.y;

        this.titleText.x = this.config[orientation].title.x;
        this.titleText.y = this.config[orientation].title.y;

        this.mobileText.x = this.config[orientation].mobile.x;
        this.mobileText.y = this.config[orientation].mobile.y;

        this.loadingGroup.x = this.config[orientation].loading.x;
        this.loadingGroup.y = this.config[orientation].loading.y;
    }

}
