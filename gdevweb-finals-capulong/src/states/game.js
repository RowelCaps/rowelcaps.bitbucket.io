class Game extends Phaser.State {
    constructor() {
        super();
        
        this.config = {
            landscape: {
                background: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.5, 
                    angle: 90
                },
                installButton: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.1,
                    scaleX: 1,
                    scaleY: 1.2
                },
                endCard: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.5,
                    scaleX: 1,
                    scaleY: 1.2
                },

                Logo:{
                    x: 0.5 * game.baseResolution.max,  
                    y: 0.2 * game.baseResolution.min,
                    scaleX: 1,
                    scaleY: 1
                },

                Tower1:{
                    x: 0.15 * game.baseResolution.max,  
                    y: 0.8 * game.baseResolution.min,
                    scaleX: 0.7,
                    scaleY: 1.2
                },

                Tower2:{
                    x: 0.5  * game.baseResolution.max,  
                    y: 0.8 * game.baseResolution.min,
                    scaleX: 0.7,
                    scaleY: 1.2
                },

                Tower3:{
                    x: 0.8 * game.baseResolution.max,
                    y: 0.8 * game.baseResolution.min,
                    scaleX: 0.7,
                    scaleY: 1.2
                },

                Donut: {
                    scaleMultiplier: 1,
                    Offset: 50
                }
            },
            portrait: {
                background: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.5,
                    angle: 0
                },
                installButton: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.05,
                    scaleX: 1,
                    scaleY: 1
                },
                endCard: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.5,
                    scaleX: 1,
                    scaleY: 1
                },

                Logo:{
                    x: 0.5 * game.baseResolution.min,  
                    y: 0.2 * game.baseResolution.max,
                    scaleX: 0.8,
                    scaleY: 0.8
                },

                Tower1:{
                    x: 0.2 * game.baseResolution.min,  
                    y: 0.8 * game.baseResolution.max,
                    scaleX: 0.7,
                    scaleY: 0.8
                },

                Tower2:{
                    x: 0.5  * game.baseResolution.min,  
                    y: 0.8 * game.baseResolution.max,
                    scaleX: 0.7,
                    scaleY: 1
                },

                Tower3:{
                    x: game.baseResolution.min * 0.8,
                    y: game.baseResolution.max * 0.8,
                    scaleX: 0.7,
                    scaleY: 1
                },

                Donut: {
                    scaleMultiplier: 0.8,
                    Offset: 35
                }
            }
        }

        this.currentSelectedDonut = null;
        this.towerDonutOffset = 50;
    }

    preload() {

        game.load.image('logo', 'assets/logo.png');
        game.load.image('donutblue', 'assets/Donut_Blue.png');
        game.load.image('donutorange', 'assets/Donut_Orange.png');
        game.load.image('donutpink', 'assets/Donut_Pink.png');
        game.load.image('donutpurple', 'assets/Donut_Purple.png');
        game.load.image('tower', 'assets/Tower.png');
    }

    create() {
        // game container (for resizing)
        game.container = game.world.addChild (new Phaser.Group (game));
        game.container.reorientLayout = this.reorientLayout.bind(this);

        // game container
        this.container = new Phaser.Group (game);
        game.container.add (this.container);

        // background
        this.background = this.container.add (this.game.add.graphics());
        this.background.beginFill (0x03fca9);
        this.background.drawRect (-game.baseResolution.min*0.5, -game.baseResolution.max*0.5, game.baseResolution.min, game.baseResolution.max);
        this.background.endFill ();
        this.background.anchor.setTo (0.5, 0.5);

        this.logo = this.container.add
                    (this.game.add.sprite(0.7 * game.baseResolution.min,
                         0.2 * game.baseResolution.max,
                         'logo'));

        this.logo.anchor.setTo(0.5,0.5);

        this.LeftTower = new Tower(this.game.add.sprite(
                0.2 * game.baseResolution.min,
                 0.8 * game.baseResolution.max,
                 'tower'));
        
        this.container.add(this.LeftTower.towerSprite);

        this.LeftTower.towerSprite.anchor.setTo(0.5,0.7);
        this.LeftTower.towerSprite.position.setTo(0.2 * game.baseResolution.min,  0.55 * game.baseResolution.max);
        this.LeftTower.towerSprite.scale.setTo(0.7,1.2);

        this.LeftTower.towerSprite.inputEnabled = true;
        this.LeftTower.towerSprite.events.onInputDown.add(function() {
            this.SelectDonuts(this.LeftTower);
        },this);
        
        this.MiddleTower = new Tower(this.game.add.sprite(
            0.2 * game.baseResolution.min,
             0.8 * game.baseResolution.max,
             'tower'));
    
        this.container.add(this.MiddleTower.towerSprite);

        this.MiddleTower.towerSprite.inputEnabled = true;
        this.MiddleTower.towerSprite.events.onInputDown.add(function() {
            this.SelectDonuts(this.MiddleTower);
        },this);

        this.MiddleTower.towerSprite.anchor.setTo(0.5,0.7);
        this.MiddleTower.towerSprite.position.setTo(0.7  * game.baseResolution.min,  0.55 * game.baseResolution.max);
        this.MiddleTower.towerSprite.scale.setTo(0.7,1.2);

        this.RightTower = new Tower(this.game.add.sprite(
            0.2 * game.baseResolution.min,
             0.8 * game.baseResolution.max,
             'tower'));
    
        this.container.add(this.RightTower.towerSprite);
        
        this.RightTower.towerSprite.inputEnabled = true;
        this.RightTower.towerSprite.events.onInputDown.add(function() {
            this.SelectDonuts(this.RightTower);
        },this);

        this.RightTower.AddOnTowerCompleteEvent("win", this);

        this.RightTower.towerSprite.anchor.setTo(0.5,0.7);
        this.RightTower.towerSprite.position.setTo(0.8 * game.baseResolution.min,  0.55 * game.baseResolution.max);
        this.RightTower.towerSprite.scale.setTo(0.7,1.2);

        this.Donut1 = new Donut(4, this.game.add.sprite(
            0.5 * game.baseResolution.min,
            0.5 * game.baseResolution.max,
            'donutpink'),
             0.7, 0.4);
        
        this.container.add(this.Donut1.donutSprite);    
        this.LeftTower.AddDonut(this.Donut1);

        this.Donut1.donutSprite.anchor.setTo(0.5,0.5);
        this.Donut1.donutSprite.position.setTo(0.2 * game.baseResolution.min, this.LeftTower.towerSprite.position.y);
        this.Donut1.donutSprite.scale.setTo(0.7,  0.4);

        this.Donut2 = new Donut(3, this.game.add.sprite(
            0.5 * game.baseResolution.min,
            0.5 * game.baseResolution.max,
            'donutblue'),
            0.6,0.4);
        
        this.container.add(this.Donut2.donutSprite);   
        this.LeftTower.AddDonut(this.Donut2); 

        this.Donut2.donutSprite.anchor.setTo(0.5,0.5);
        this.Donut2.donutSprite.position.setTo(0.2 * game.baseResolution.min, this.LeftTower.towerSprite.position.y - this.towerDonutOffset);
        this.Donut2.donutSprite.scale.setTo(0.6,  0.4);

        this.Donut3 = new Donut(2, this.game.add.sprite(
            0.5 * game.baseResolution.min,
            0.5 * game.baseResolution.max,
            'donutorange'),
            0.5,0.4);
        
        this.container.add(this.Donut3.donutSprite); 
        this.LeftTower.AddDonut(this.Donut3);   

        this.Donut3.donutSprite.anchor.setTo(0.5,0.5);
        this.Donut3.donutSprite.position.setTo(0.2 * game.baseResolution.min, this.LeftTower.towerSprite.position.y - this.towerDonutOffset *2) ;
        this.Donut3.donutSprite.scale.setTo(0.5,  0.4);

        this.Donut4 = new Donut(1, this.game.add.sprite(
            0.5 * game.baseResolution.min,
            0.5 * game.baseResolution.max,
            'donutpurple'),
            0.4,0.4);
        
        this.container.add(this.Donut4.donutSprite);
        this.LeftTower.AddDonut(this.Donut4);    

        this.Donut4.donutSprite.anchor.setTo(0.5,0.5);
        this.Donut4.donutSprite.position.setTo(0.2 * game.baseResolution.min, this.LeftTower.towerSprite.position.y - this.towerDonutOffset  * 3);
        this.Donut4 .donutSprite.scale.setTo(0.4,  0.4);

        // install button at top
        this.installButton = new InstallButton (game);
        this.container.addChild (this.installButton);

        this.endCard = new EndCard (game);
        
        // update orientation
        adaptGameToOrientation();
        assignOrientationChangeHandlers();

        this.gameStart ();
    }

    SelectDonuts(tower)
    {
        if(tower.donuts.includes(this.currentSelectedDonut)){
            this.currentSelectedDonut = null;
            return;
        }
        
        if(this.currentSelectedDonut == null && tower.donuts.length > 0){
            this.currentSelectedDonut = tower.donuts[tower.donuts.length - 1];
            return;
        }    

        if(tower.validDonut(this.currentSelectedDonut)) {

            if(this.currentSelectedDonut.tower != null)
                this.currentSelectedDonut.tower.donuts.pop();
                
            tower.AddDonut(this.currentSelectedDonut);

            this.currentSelectedDonut.donutSprite.position.setTo(
                tower.towerSprite.position.x,
                tower.towerSprite.position.y - this.towerDonutOffset * (tower.donuts.length - 1));

            this.currentSelectedDonut = null;
        } 
        else{
            this.currentSelectedDonut = null;
        }
    }

    gameStart () {
        // add a delay timer that will trigger game over
        game.time.events.add(Phaser.Timer.SECOND * 5, function () {
            //this.gameOver ("win");
        }, this);
    }

    gameOver (condition) {
        // hide the ins tall button at the top
        this.installButton.visible = false;

        // create a transparent black background to dim the game
        this.createDimOverlay ();

        // add the end card on top of the game
        this.container.addChild (this.endCard);

        // pass in the condition to the end card to modify the header text
        this.endCard.show (condition);
    }

    createDimOverlay () {
        this.overlay = this.container.add (this.game.add.graphics());
        this.overlay.beginFill (0x000000, 0.5);
        this.overlay.drawRect (-game.baseResolution.max*0, -game.baseResolution.max*0, game.baseResolution.max, game.baseResolution.max);
        this.overlay.endFill ();
    }

    reorientLayout (orientation) {
        this.orient = orientation;

        this.background.angle = this.config[orientation].background.angle;
        this.background.x = this.config[orientation].background.x;
        this.background.y = this.config[orientation].background.y;

        this.installButton.x = this.config[orientation].installButton.x;
        this.installButton.y = this.config[orientation].installButton.y;
        this.installButton.scale.x = this.config[orientation].installButton.scaleX;
        this.installButton.scale.y = this.config[orientation].installButton.scaleY;

        this.endCard.x = this.config[orientation].endCard.x;
        this.endCard.y = this.config[orientation].endCard.y;
        this.endCard.scale.x = this.config[orientation].endCard.scaleX;
        this.endCard.scale.y = this.config[orientation].endCard.scaleY;

        this.logo.position.x = this.config[orientation].Logo.x;
        this.logo.position.y = this.config[orientation].Logo.y;
        this.logo.scale.x = this.config[orientation].Logo.scaleX;
        this.logo.scale.y = this.config[orientation].Logo.scaleY;

        this.LeftTower.towerSprite.position.x = this.config[orientation].Tower1.x;
        this.LeftTower.towerSprite.position.y = this.config[orientation].Tower1.y;
        this.LeftTower.towerSprite.scale.x = this.config[orientation].Tower1.scaleX;
        this.LeftTower.towerSprite.scale.y = this.config[orientation].Tower1.scaleY;

        this.LeftTower.OnResize(this.config[orientation].Donut.scaleMultiplier,
            this.config[orientation].Donut.Offset);

        this.MiddleTower.towerSprite.position.x = this.config[orientation].Tower2.x;
        this.MiddleTower.towerSprite.position.y = this.config[orientation].Tower2.y;
        this.MiddleTower.towerSprite.scale.x = this.config[orientation].Tower2.scaleX;
        this.MiddleTower.towerSprite.scale.y = this.config[orientation].Tower2.scaleY;


        this.MiddleTower.OnResize(this.config[orientation].Donut.scaleMultiplier,
            this.config[orientation].Donut.Offset);

        this.RightTower.towerSprite.position.x = this.config[orientation].Tower3.x;
        this.RightTower.towerSprite.position.y = this.config[orientation].Tower3.y;
        this.RightTower.towerSprite.scale.x = this.config[orientation].Tower3.scaleX;
        this.RightTower.towerSprite.scale.y = this.config[orientation].Tower3.scaleY;

        this.RightTower.OnResize(this.config[orientation].Donut.scaleMultiplier,
            this.config[orientation].Donut.Offset);
    }
}
