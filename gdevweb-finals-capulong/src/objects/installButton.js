class InstallButton extends Phaser.Button {
    constructor(game) {

        // button texture
        var width = game.baseResolution.min * 0.5;
        var height = game.baseResolution.min * 0.1;
        var texture = game.add.graphics();
        texture.beginFill (0x006600);
        texture.drawRect (0, 0, width, height);
        texture.endFill ();

        // event listener
        var onClick = function () {        
            window.open("https://www.msn.com","_self");
        }

        super(game, 0, 0, texture.generateTexture(), onClick);
        
        // destroy the graphics texture since it's already been used for the button texture
        texture.destroy ();

        this.anchor.setTo (0.5, 0.5)

        this.init ();
    }

    init () {
        // text
        var style = {
            font: 'Arial',
            fontSize: 26,
            fill: "#ffffff",
            align: "center",
            fontWeight: 600
        };

        this.text = this.addChild (this.game.add.text (0, 0, game.strings["INSTALL_NOW"], style));
        this.text.anchor.setTo (0.5, 0.5);
    }
}
