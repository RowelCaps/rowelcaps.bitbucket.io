class EndCard extends Phaser.Group {
    constructor(game) {
        super(game);

        this.init ();

        this.visible = false;
    }

    init () {
        // end card frame
        var width = game.baseResolution.max * 0.5;
        var height = game.baseResolution.max * 0.5;
        this.frame = this.add (this.game.add.graphics());
        this.frame.beginFill (0x000066);
        this.frame.drawRect (-width * 0.5, -height * 0.5, width, height);
        this.frame.endFill ();

        // header text
        var style = {
            font: 'Arial',
            fontSize: 32,
            fill: "#ffffff",
            align: "center",
            fontWeight: 600
        };

        this.headerText = this.add (this.game.add.text (0, -game.baseResolution.max * 0.18, "", style));
        this.headerText.anchor.setTo (0.5, 0.5);

        // body text
        style.fontWeight = 0;
        style.fontSize = 24;

        this.bodyText = this.add (this.game.add.text (0, 0, game.strings["ENDCARD_BODY"], style));
        this.bodyText.anchor.setTo (0.5, 0.5);   

        // install button
        this.installButton = new InstallButton (game);
        this.installButton.position.setTo (0, game.baseResolution.max * 0.18);
        this.addChild (this.installButton);
    }

    show (condition) {
        this.headerText.text = (condition == "win") ? game.strings["ENDCARD_HEADER_WON"] : game.strings["ENDCARD_HEADER_LOST"];
        this.visible = true;
    }
}
