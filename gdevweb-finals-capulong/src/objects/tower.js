class Tower{

    constructor(sprite){
        this.donuts = [];
        this.towerSprite = sprite;
        this.gamestate = null;
        this.winText;
    }

    GetDonutNumber()
    {
        return this.donuts.length;
    }


    AddDonut(Donut){


        Donut.tower = this;
        this.donuts.push(Donut);

        if(this.donuts.length >= 4 && this.gamestate != null) 
            this.gamestate.gameOver (this.winText);
    }

    validDonut(Donut){

        if(this.donuts.length <= 0 ) 
            return true;

        if(this.donuts[this.donuts.length - 1].donutSize > Donut.donutSize &&
            !this.donuts.includes(Donut))
            return true;

        return false;
    }

    AddOnTowerCompleteEvent(win, gamestate)
    {
        this.winText = win;
        this.gamestate = gamestate;
    }

    OnResize(scaleMultiplier, offset)
    {
        if(this.donuts.length <= 0)
            return;

        for(var i = 0; i < this.donuts.length; i++){
            var donut = this.donuts[i];
            
            donut.donutSprite.position.setTo(
                this.towerSprite.position.x,
                 this.towerSprite.position.y - offset * i);
            
                 console.log("Fuck you");
            donut.donutSprite.scale.x = donut.IScaleX * scaleMultiplier;  
            donut.donutSprite.scale.y = donut.IScaleY * scaleMultiplier;       
        }    

    }
}